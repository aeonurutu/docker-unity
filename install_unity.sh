#!/bin/bash

UNITY_INSTALLER_LOC=$1

wget $UNITY_INSTALLER_LOC -O /unity_installer.sh
echo -ne '\n' | sh /unity_installer.sh
rm /unity_installer.sh
mv unity-editor-* unity-editor
