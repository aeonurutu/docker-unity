# Copyright 2016-2017 AeonUrutu LLC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM ubuntu:latest

RUN apt-get -y update && apt-get -y install \
	bzip2 \
	git \
	libasound2 \
	libgconf-2-4 \
	libglu1-mesa \
	libgtk2.0-0 \
	libnss3 \
	libpq5 \
	libxtst6 \
	python3 \
	wget

LABEL maintainer=contact@aeonurutu.com
LABEL source=https://bitbucket.org/aeonurutu/docker-unity
ARG unity_installer_loc=http://beta.unity3d.com/download/8bc04e1c171e/unity-editor-installer-5.6.0xf3Linux.sh

ADD install_unity.sh install_unity.sh
RUN /install_unity.sh $unity_installer_loc && rm /install_unity.sh

ADD ./unity_wrapper.py unity_wrapper.py
ENTRYPOINT ["/unity_wrapper.py"]
