# Docker Unity Image

Creates a Docker image that when run pulls a git branch and runs Unity commands against it.

## Usage

The following example clones the _bugfix_ branch from _joebloggs_'s repo _unity-project_ and runs the editor tests against it:

```Shell
$ docker run aeonurutu/unity5.6 \
https://bitbucket.org/joebloggs/unity-project.git \
bugfix \
--username 'JoeBloggs@example.com' \
--password 'MyPassw0rd' \
--runEditorTests
```

Full documentation on supported comamnds can be viewed by passing the '--help' argument.

```Shell
$ docker run aeonurutu/unity5.6 --help
usage: unity_wrapper.py [-h] [--dryrun] [--runEditorTests]
                        [--username USERNAME] [--password PASSWORD]
                        repo [branch]

positional arguments:
  repo                 Git repo to clone branch from
  branch               branch to run unity against, defaults to master

optional arguments:
  -h, --help           show this help message and exit
  --dryrun             don't actually execute the commands
  --runEditorTests     Run Editor tests from the project.
  --username USERNAME  Enter a username into the log-in form during activation
                       of the Unity Editor.
  --password PASSWORD  The password of the user, required when launching.
```

## Building

To build using the deafult version of Unity, a simple "docker build ." is all that is required.

If you prefer to specify the location/version of Unity, you can provide a URL to it via the build arg _unity_installer_loc_.

```Shell
$ docker build . \
--build-arg unity_installer_loc=http://beta.unity3d.com/download/e348e673a4c6/unity-editor-installer-5.6.0xf1Linux.sh
$ docker tag 48a7749ede29 aeonurutu/unity5.6
```

## TODO

[ ] Figure out why results of runEditorTests are not saved

[ ] Write a better description for argparse in unity_wrapper.py

[ ] When supplying the runEditorTests flag, the results should be displayed.  Additionally, if there were errors, unity_wrapper.py should exit with a non-zero error code

[ ] Support for more cli commands should be added, specifically the -build* flags

[ ] There should be a mechanism to offload the logs from the Unity command somewhere

[ ] I suspect it is a bad idea to pass username & password via the run command.  Investigate this

[ ] Determine what the best method to deploy keys to the image such that projects can be cloned from private repos

[X] ~~Figure out how to reduce the size of the image~~

## Tips

If you already have a Untiy Editor installer script downloaded, you can save time by serving it to Docker using Python as follows:

```Shell
$ cd dir_with_editor
$ python -m SimpleHTTPServer
Serving HTTP on 0.0.0.0 port 8000 ...
```

Then, if your host IP is 192.168.1.42, run your build command as follows:

```Shell
$ docker build . \
--build-arg unity_installer_loc=http://192.168.1.42:8000/unity-editor-installer-5.6.0xf3Linux.sh
```

## Troubleshooting

If you start getting weird errors reporting lack of disk space, check your Docker.qcow2 file, you might be running into a [bug](https://github.com/docker/for-mac/issues/371) with Docker.