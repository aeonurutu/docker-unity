#!/usr/bin/env python3
# Copyright 2016-2017 AeonUrutu LLC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import os
import subprocess


def exec_cmd(cmd, dryrun):
    """Execute a command unles dryrun is True.

    Keyword arguments:
    cmd -- command to be executed
    dryrun -- if True, return before cmd is executed
    """
    print(' '.join(cmd))
    if dryrun:
        return
    process = subprocess.run(cmd)


def clone_repo(repo, branch, dryrun=False, **kwargs):
    """Clone a branch from a Git repo.

    Keyword arguments:
    repo -- the git repo to be cloned
    branch -- the branch to clone from the repo
    dryrun -- don't actually execute the commands
    """
    cmd = ['git', 'clone', '--single-branch', '-b', branch, repo,
           'project_dir']
    exec_cmd(cmd, dryrun)


def exec_unity(dryrun=False, **kwargs):
    """Execute Unity in CLI mode.

    Keyword arguments:
    dryrun -- don't actually execute the commands
    """
    cmd = ['./unity-editor/Editor/Unity',
           '-batchmode',
           '-nographics',
           '-quit',
           '-projectPath', 'project_dir']
    for k, v in kwargs.items():
        cmd.append('-{}'.format(str(k)))
        if not isinstance(v, bool):
            cmd.append(str(v))
    if 'runEditorTests' in kwargs:
        cmd.append('-editorTestsResultFile')
        cmd.append('./')
    exec_cmd(cmd, dryrun)


def main():
    def description():
        return ''

    parser = argparse.ArgumentParser(description=description())
    parser.add_argument('repo', nargs=1, help='Git repo to clone branch from')
    parser.add_argument('branch', nargs='?', default='master',
                        help='branch to run unity against, defaults to master')
    parser.add_argument('--dryrun', default=False, action='store_true',
                        help='don\'t actually execute the commands')
    parser.add_argument('--runEditorTests', default=False, action='store_true',
                        help='Run Editor tests from the project.')

    parser.add_argument('--username', default='',
                        help=('Enter a username into the log-in form during '
                              'activation of the Unity Editor.'))
    parser.add_argument('--password', default='',
                        help=('The password of the user, required when '
                              'launching.'))

    args = vars(parser.parse_args())

    print('Unity Wrapper')
    print('Copyright 2016-2017 AeonUrutu LLC.')

    repo = args.get('repo')[0]
    branch = args.get('branch')
    del args['repo']
    del args['branch']

    if args.get('dryrun', False):
        print('Dryrun: the following commands would have been executed:')
    clone_repo(repo, branch, **args)
    exec_unity(**args)


if __name__ == '__main__':
    main()
